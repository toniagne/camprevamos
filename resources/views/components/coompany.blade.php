<div class="product-area section-space-y-axis-50">
    <div class="container">

        <div class="row">
            <div class="col-lg-4 pt-7 pt-lg-0">
                <div class="product-banner img-hover-effect">
                    <div class="product-banner-img fixed-height img-zoom-effect">
                        <a href="shop.html">
                            <img class="img-full" src="{{ asset('images/background_institucional.png') }}" alt="Product Banner">
                        </a>
                        <div class="product-banner-content text-white">
                            <div class="row">
                                <div class="col-sm-2">
                                    <i class="fa fa-trailer"></i>
                                </div>
                                <div class="col-sm-10 ">
                                        <h2 class="category">CONFORTO & PRATICIDADE</h2>
                                        <p class="short-desc mb-7 m-4"> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-2">
                                    <i class="fa fa-trailer"></i>
                                </div>
                                <div class="col-sm-10 ">
                                    <h2 class="category">ACESSÍVEL</h2>
                                    <p class="short-desc mb-7 m-4"> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-2">
                                    <i class="fa fa-trailer"></i>
                                </div>
                                <div class="col-sm-10 ">
                                    <h2 class="category">LIBERDADE</h2>
                                    <p class="short-desc mb-7 m-4"> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-sm-12 ">
                                    <p class="short-desc mb-7 m-4"> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </p>
                                </div>
                            </div>

                       </div>


                    </div>
                </div>
            </div>
            <div class="col-lg-8">
                <div class="product-item-wrap row">
                    <div class="col-lg-12 col-md-3">
                        <div class="product-item">
                            <div class="product-img img-zoom-effect">
                                <a href="shop.html">
                                    <img class="img-full" src="assets/images/product/medium-size/special-deals/1-1-620x350.jpg" alt="Product Images">
                                </a>
                            </div>
                            <div class="product-content">
                                <a class="product-name pb-1" href="shop.html">Auto Clutch & Brake</a>
                                <div class="price-box">
                                    <div class="price-box-holder">
                                        <span>Price:</span>
                                        <span class="new-price text-primary">$120.00</span>
                                    </div>
                                </div>
                                <div class="product-add-action">
                                    <ul>
                                        <li>
                                            <a href="cart.html" data-tippy="Add to cart" data-tippy-inertia="true" data-tippy-animation="shift-away" data-tippy-delay="50" data-tippy-arrow="true" data-tippy-theme="sharpborder">
                                                <i class="pe-7s-cart"></i>
                                            </a>
                                        </li>
                                        <li class="quuickview-btn" data-bs-toggle="modal" data-bs-target="#quickModal">
                                            <a href="#" data-tippy="Quickview" data-tippy-inertia="true" data-tippy-animation="shift-away" data-tippy-delay="50" data-tippy-arrow="true" data-tippy-theme="sharpborder">
                                                <i class="pe-7s-look"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="wishlist.html" data-tippy="Add to wishlist" data-tippy-inertia="true" data-tippy-animation="shift-away" data-tippy-delay="50" data-tippy-arrow="true" data-tippy-theme="sharpborder">
                                                <i class="pe-7s-like"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="compare.html" data-tippy="Add to compare" data-tippy-inertia="true" data-tippy-animation="shift-away" data-tippy-delay="50" data-tippy-arrow="true" data-tippy-theme="sharpborder">
                                                <i class="pe-7s-shuffle"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-3 pt-7">
                        <div class="product-item">
                            <div class="product-img img-zoom-effect">
                                <a href="shop.html">
                                    <img class="img-full" src="assets/images/product/medium-size/special-deals/1-1-620x350.jpg" alt="Product Images">
                                </a>
                            </div>
                            <div class="product-content">
                                <a class="product-name pb-1" href="shop.html">Auto Clutch & Brake</a>
                                <div class="price-box">
                                    <div class="price-box-holder">
                                        <span>Price:</span>
                                        <span class="new-price text-primary">$120.00</span>
                                    </div>
                                </div>
                                <div class="product-add-action">
                                    <ul>
                                        <li>
                                            <a href="cart.html" data-tippy="Add to cart" data-tippy-inertia="true" data-tippy-animation="shift-away" data-tippy-delay="50" data-tippy-arrow="true" data-tippy-theme="sharpborder">
                                                <i class="pe-7s-cart"></i>
                                            </a>
                                        </li>
                                        <li class="quuickview-btn" data-bs-toggle="modal" data-bs-target="#quickModal">
                                            <a href="#" data-tippy="Quickview" data-tippy-inertia="true" data-tippy-animation="shift-away" data-tippy-delay="50" data-tippy-arrow="true" data-tippy-theme="sharpborder">
                                                <i class="pe-7s-look"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="wishlist.html" data-tippy="Add to wishlist" data-tippy-inertia="true" data-tippy-animation="shift-away" data-tippy-delay="50" data-tippy-arrow="true" data-tippy-theme="sharpborder">
                                                <i class="pe-7s-like"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="compare.html" data-tippy="Add to compare" data-tippy-inertia="true" data-tippy-animation="shift-away" data-tippy-delay="50" data-tippy-arrow="true" data-tippy-theme="sharpborder">
                                                <i class="pe-7s-shuffle"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
</div>
