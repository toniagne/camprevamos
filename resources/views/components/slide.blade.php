<div class="swiper-container main-slider swiper-arrow with-bg_white">
    <div class="swiper-wrapper">
        <div class="swiper-slide animation-style-01">
            <div class="slide-inner bg-height" data-bg-image="{{ asset('images/slide1.jpeg') }}">
                <div class="container">
                    <div class="slide-content text-white">
                        <h3 class="sub-title">Big Sale Offer</h3>
                        <h2 class="title mb-3">Parts & repair</h2>
                        <p class="short-desc different-width mb-10">Exclusive Exchange Offer & 30% Off In This Week</p>
                        <div class="button-wrap">
                            <a class="btn btn-custom-size lg-size btn-primary" href="shop.html">Shop Now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- Add Pagination -->
    <div class="swiper-pagination with-bg d-md-none"></div>

    <!-- Custom Arrows -->
    <div class="custom-arrow-wrap d-none d-md-block">
        <div class="custom-button-prev"></div>
        <div class="custom-button-next"></div>
    </div>
</div>

<section class="search-sec" style="z-index: 9999;">
    <div class="container">
        <form action="#" method="post" novalidate="novalidate">
            <div class="row">
                <div class="col-lg-12">
                     <h5 class="title"> <i class="fa fa-car"></i> VAMOS DE MOTORHOME ?</h5>
                    <h6>Experimente a magia do ar livre com o melhor aluguel de RV, van, trailer de viagem imaginável</h6>
                </div>
            </div>
            <div class="row mt-3">
                <div class="col-sm-4">
                    <input type="text" placeholder="Digite a cidade ou localidade" class="form-control bg-light flex">
                </div>
                <div class="col-sm-3">
                    <input type="date" placeholder="Check-in" class="form-control bg-light flex">
                </div>
                <div class="col-sm-3">
                    <input type="date" placeholder="Check-out" class="form-control bg-light flex">
                </div>
                <div class="col-sm-2">
                    <button type="submit" class="btn btn-dark"> Pesquisar </button>
                </div>
            </div>
        </form>
    </div>
</section>



